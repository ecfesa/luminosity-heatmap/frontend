import "./App.css";
import Heatmap from "./components/Heatmap";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { Typography, Container, Grid, Box, IconButton } from "@mui/material";
import ConsumeData from "./components/LogData";
import LightIcon from "@mui/icons-material/Light";
import { yellow } from "@mui/material/colors";
import { useEffect, useState } from "react";
import useLampData from "./utils/useLampData";

function App() {
  const [isDarkMode, setIsDarkMode] = useState(false);
  const { lamps, loading, error } = useLampData();
  const [data, setData] = useState(lamps); // manipulate tha lamps data from api to pass it to the components

  useEffect(() => {
    if (!loading && !error) {
      setData(lamps);
    }
  }, [lamps, loading, error]);

  const lightTheme = createTheme({
    typography: {
      fontFamily: "Inter",
    },
    palette: {
      mode: "light",
      primary: yellow,
      secondary: {
        main: "#282828",
      },
      text: {
        primary: "#282828",
        secondary: "#282828",
      },
      background: {
        default: "#fae691",
        paper: "#fceba4",
      },
      divider: "#3d3d3d",
    },
  });

  const darkTheme = createTheme({
    typography: {
      fontFamily: "Inter",
    },
    palette: {
      mode: "dark",
      primary: yellow,
      secondary: yellow,
      text: {
        primary: yellow[400],
        secondary: "#ececec",
      },
      background: {
        default: "#282828",
        paper: "#2e2d2d",
      },
      divider: "#fceba4",
    },
  });

  return (
    <ThemeProvider theme={isDarkMode ? darkTheme : lightTheme}>
      <CssBaseline />

      <Box sx={{ width: "100%", height: "100%" }}>
        <Box sx={{ display: "flex", justifyContent: "flex-end", pt: 2, pr: 2 }}>
          <IconButton onClick={() => setIsDarkMode(!isDarkMode)}>
            <LightIcon sx={{ color: isDarkMode && "text.primary" }} />
          </IconButton>
        </Box>
        <Container maxWidth="98">
          <Grid container spacing={3} justifyContent="center">
            <Grid item xs={12}>
              <Typography
                variant="h3"
                align="center"
                fontWeight={800}
                sx={{ paddingBottom: 2 }}
              >
                Luminosity Heatmap
              </Typography>
            </Grid>

            <Grid
              item
              sx={{
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Heatmap
                lamps={data}
                isLoading={loading}
                error={error}
                sensorLimit={50}
              />
            </Grid>
            <Grid item>
              <ConsumeData lamps={data} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </ThemeProvider>
  );
}

export default App;
