import { useState, useEffect } from "react";

const useLampData = () => {
  const [lamps, setLamps] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchDevices();
  }, []);

  useEffect(() => {
    const intervalId = setInterval(fetchDevices, 20000); // fetch every 2 seconds
    return () => clearInterval(intervalId);
  }, []);

  const fetchDevices = async () => {
    const fiwareAddress = "localhost"; // change to machine ip
    const url = `http://${fiwareAddress}:5000/api/lamps/averages`;

    try {
      setLoading(true);
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`Error fetching devices: ${response.status}`);
      }
      const data = await response.json();
      setLamps(data);
    } catch (error) {
      setError(error);
      console.error(error);
    } finally {
      setLoading(false);
    }
  };
  return { lamps, loading, error };
};

export default useLampData;
