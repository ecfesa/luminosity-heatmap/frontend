export default function getLastMinute() {
	const current = new Date();
	current.setUTCMinutes(current.getUTCMinutes() - 1);
	return current.toISOString();
}
