import getLastMinute from './getLastMinute';

export default async function getMinuteAverage() {
	const timeFrom = getLastMinute();
	const fiwareAddress = 'localhost';
	const entityName = 'urn:ngsi-ld:Lamp:005';
	const url = `http://${fiwareAddress}:8666/STH/v1/contextEntities/type/Lamp/id/${entityName}/attributes/luminosity?dateFrom=${timeFrom}&lastN=100`;

	const headers = {
		'fiware-service': 'smart',
		'fiware-servicepath': '/',
	};

	try {
		const response = await fetch(url, { headers });
		if (!response.ok) {
			throw new Error(`Error fetching data: ${response.status}`);
		}
		const data = await response.json();
		const luminosityValues =
			data.contextResponses[0].contextElement.attributes[0].values.map(
				(value) => value.attrValue
			);
		return luminosityValues.length
			? luminosityValues.reduce((acc, val) => acc + val, 0) /
					luminosityValues.length
			: 0;
	} catch (error) {
		console.error(error);
		return 0;
	}
}
