import LightIcon from "@mui/icons-material/Light";
import { Tooltip } from "@mui/material";

const LightIconComponent = ({ value, sensorLimit, top, left }) => (
  <Tooltip title={value}>
    <LightIcon
      sx={{
        color: value > sensorLimit ? "black" : "white",
        position: "relative",
        top: `${top}px`,
        left: `${left}px`,
        zIndex: 1,
      }}
    />
  </Tooltip>
);

export default LightIconComponent;
