import { Box, Typography } from "@mui/material";
import LightIcon from "@mui/icons-material/Light";

function LogData({ lamps }) {
  return (
    <Box
      sx={{
        alignSelf: "center",
        justifyContent: "center",
        width: "400px",
        overflow: "hidden",
        borderColor: "divider",
      }}
    >
      <Typography
        variant="h4"
        fontWeight={600}
        sx={{
          paddingBottom: 1,
          paddingLeft: 2,
          paddingTop: 4,
          color: "text.secondary",
        }}
      >
        Automatic Efficiency
      </Typography>
      <Typography
        fontWeight={400}
        sx={{
          paddingBottom: 0,
          paddingLeft: 2,
          paddingTop: 2,
          color: "text.secondary",
        }}
      >
        This heatmap helps identify zones with the highest lamp intensity. Use
        this information to optimize energy consumption.
      </Typography>
      <Typography
        fontWeight={400}
        sx={{
          paddingLeft: 2,
          paddingTop: 1,
          color: "text.secondary",
        }}
      >
        <LightIcon sx={{ marginTop: 2, color: "white" }} /> indicates a lamp
        with intensity below the sensor limit (the light is on)
      </Typography>
      <Typography
        fontWeight={400}
        sx={{
          paddingBottom: 1,
          paddingLeft: 2,
          paddingTop: 1,
          color: "text.secondary",
        }}
      >
        <LightIcon sx={{ marginTop: 2, color: "black" }} /> indicates a lamp
        with intensity above the sensor limit (the light is off)
      </Typography>
      <div>
        <ul>
          {/* considering the format
                {
        "urn:ngsi-ld:Lamp:001": 43.3131463535,
        "urn:ngsi-ld:Lamp:002": 45.03302854408333,
        "urn:ngsi-ld:Lamp:003": 51.31972416391667,
        "urn:ngsi-ld:Lamp:004": 56.39308373516667,
        "urn:ngsi-ld:Lamp:005": 55.58752363758333,
        "urn:ngsi-ld:Lamp:006": 49.64422455366667,
        "urn:ngsi-ld:Lamp:007": 44.02824693033333,
        "urn:ngsi-ld:Lamp:008": 43.90322497008333
    }

          present this data in a user friendly way      
                
                */}
          {Object.entries(lamps).map(([key, value]) => {
            return (
              <li key={key}>
                <Typography
                  fontWeight={400}
                  sx={{
                    color: "text.secondary",
                    paddingBottom: 1,
                  }}
                >
                  {key.split(":")[2]}
                  {key.split(":")[3]}: {value.toFixed(2)}
                  {"%"}
                </Typography>
              </li>
            );
          })}
        </ul>
      </div>
    </Box>
  );
}

export default LogData;
