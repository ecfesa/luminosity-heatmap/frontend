import React, { useEffect, useRef, useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import h337 from "heatmap.js";
import { Box, Typography } from "@mui/material";
import useTheme from "@mui/system/useTheme";
import CircularProgress from "@mui/material/CircularProgress";
import LightIconComponent from "./LightBulbComponent";

const Heatmap = ({ lamps, isLoading, error, sensorLimit }) => {
  const heatmapContainer = useRef(null);
  const theme = useTheme();
  const [values, setValues] = useState([]);
  const [open, setOpen] = useState(false);

  // the positions of the bulbs, handwritten because of heatmap lib
  const positions = [
    { top: 188, left: 264 },
    { top: 238, left: 440 },
    { top: 188, left: 615 },
    { top: 288, left: 192 },
    { top: 288, left: 568 },
    { top: 388, left: 144 },
    { top: 338, left: 320 },
    { top: 388, left: 496 },
  ];

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (isLoading) {
      return;
    }

    const createHeatmap = () => {
      const heatmapInstance = h337.create({
        container: heatmapContainer.current,
        radius: 150,
        maxOpacity: 0.5,
        minOpacity: 0,
        blur: 1,
        gradient: {
          ".5": `${theme.palette.mode === "dark" ? "black" : "blue"}`,
          ".75": `${theme.palette.mode === "dark" ? "gray" : "red"}`,
          ".9": "white",
        },
      });

      const values = Object.values(lamps);
      setValues(values);

      const points = [];
      points.push({ x: 275, y: 170, value: values[0] });
      points.push({ x: 475, y: 220, value: values[1] });
      points.push({ x: 675, y: 170, value: values[2] });
      points.push({ x: 275, y: 270, value: values[3] });
      points.push({ x: 675, y: 270, value: values[4] });
      points.push({ x: 275, y: 370, value: values[5] });
      points.push({ x: 475, y: 320, value: values[6] });
      points.push({ x: 675, y: 370, value: values[7] });

      const data = {
        max: 100,
        data: points,
      };

      heatmapInstance.setData(data);
    };

    // data example
    //   {
    //     "urn:ngsi-ld:Lamp:001": 43.3131463535,
    //     "urn:ngsi-ld:Lamp:002": 45.03302854408333,
    //     "urn:ngsi-ld:Lamp:003": 51.31972416391667,
    //     "urn:ngsi-ld:Lamp:004": 56.39308373516667,
    //     "urn:ngsi-ld:Lamp:005": 55.58752363758333,
    //     "urn:ngsi-ld:Lamp:006": 49.64422455366667,
    //     "urn:ngsi-ld:Lamp:007": 44.02824693033333,
    //     "urn:ngsi-ld:Lamp:008": 43.90322497008333
    // }

    heatmapContainer.current.innerHTML = "";

    createHeatmap();
  }, [isLoading, lamps, theme.palette.mode]);

  return (
    <Box
      sx={{ bgcolor: "background.paper", p: 2 }}
      style={{
        boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
        borderRadius: "10px",
      }}
    >
      <Box>
        {isLoading && (
          <CircularProgress
            sx={{
              marginBottom: "-20px",
              marginRight: "-40px",
              position: "relative",
              top: "0px",
              left: "0px",
              zIndex: 1,
              color: "text.secondary",
            }}
          />
        )}
        {error && (
          <Typography
            sx={{ color: "error.main", fontSize: 14, fontWeight: 600 }}
          >
            Error fetching data
          </Typography>
        )}
        {positions.map((position, index) => (
          <LightIconComponent
            key={index}
            value={values[index]}
            sensorLimit={sensorLimit}
            top={position.top}
            left={position.left}
          />
        ))}
      </Box>
      <Box
        ref={heatmapContainer}
        sx={{
          minWidth: "960px",
          minHeight: "540px",
          borderRadius: "10px",
          overflow: "hidden",
          justifyContent: "end",
        }}
      />

      {/* <Button onClick={handleClickOpen}>
        <Typography
          sx={{ fontSize: 10, fontWeight: 600 }}
          color="text.secondary"
        >
          config
        </Typography>
      </Button> */}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Data Configuration</DialogTitle>
        <DialogContent>{/* content of the dialog */}</DialogContent>
      </Dialog>
    </Box>
  );
};

export default Heatmap;
