import React, { useState } from "react";

const DataConfiguration = () => {
  const [order, setOrder] = useState("asc");

  const handleChangeOrder = () => {
    // Logic to change the order of the data
    // You can fetch the data from the API and manipulate it here
    // For example, you can sort the data in ascending or descending order

    // Update the order state
    setOrder(order === "asc" ? "desc" : "asc");
  };

  return (
    <div>
      <h2>Data Configuration</h2>
      <button onClick={handleChangeOrder}>Change Order</button>
      {/* Render the data here */}
    </div>
  );
};

export default DataConfiguration;
