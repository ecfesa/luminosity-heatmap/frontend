# Luminosity Heatmap

![Application Prinscreen](image.png)

## Introduction

The Luminosity Heatmap project is designed to visualize the distribution of luminosity in a given area. It provides a graphical representation of the intensity of light in different regions, allowing users to identify areas with high or low luminosity.

## Installation

To install and run the Luminosity Heatmap project, follow these steps:

1. Clone the repository:

```bash
git clone https://gitlab.com/ecfesa/luminosity-heatmap/frontend.git
```

2. Navigate to the project directory:

```bash
cd luminosity-heatmap
```

3. Install the dependencies:

```bash
npm install
```

4. Start the application:

```bash
npm start
```

## Usage

Once the application is running, you can access it in your web browser at `http://localhost:3000`. You must receive the information of the sensors for the application to run, for that, use the api: [https://gitlab.com/ecfesa/luminosity-heatmap/python-api](https://gitlab.com/ecfesa/luminosity-heatmap/python-api)
